package com.example.demo.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.example.demo.model.Role;

public class UserDTO implements DTOEntity{
	 
	private int id;
		
	private String name;
	
    private String email;

	private String password;

	private boolean active;
	//List<OrderDTO> orders = new ArrayList<OrderDTO>();
	
	Set<Role> roles;
	
//	public List<OrderDTO> getOrders() {
//		return orders;
//	}
//
//	public void setOrders(List<OrderDTO> orders) {
//		this.orders = orders;
//	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public UserDTO() {
		super();
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
}
