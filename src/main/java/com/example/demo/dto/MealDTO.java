package com.example.demo.dto;

public class MealDTO implements DTOEntity{

	private int id;
	
	private String name;

	private boolean preOrder;
	
	private int price;
	
	private MealSubTypeDTO mealSubType;
	
	private String description;

	private MealTypeDTO mealType;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}

	public boolean isPreOrder() {
		return preOrder;
	}
	public void setPreOrder(boolean preOrder) {
		this.preOrder = preOrder;
	}
	
	public MealSubTypeDTO getMealSubType() {
		return mealSubType;
	}
	public void setMealSubType(MealSubTypeDTO mealSubType) {
		this.mealSubType = mealSubType;
	}
	public MealTypeDTO getMealType() {
		return mealType;
	}
	public void setMealType(MealTypeDTO mealType) {
		this.mealType = mealType;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public MealDTO() {
		super();
	}
}
