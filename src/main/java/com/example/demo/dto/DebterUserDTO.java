package com.example.demo.dto;

public class DebterUserDTO implements DTOEntity{
	private int id;
	
	private UserDTO user;
	
	private int price;
	
	private boolean isPaid;
	
	public boolean isPaid() {
		return isPaid;
	}

	public void setPaid(boolean isPaid) {
		this.isPaid = isPaid;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public void setUser(UserDTO user) {
		this.user = user;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public UserDTO getUser() {
		return user;
	}

	public void Users(UserDTO user) {
		this.user = user;
	}
}
