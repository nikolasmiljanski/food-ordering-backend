package com.example.demo.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;

public class OrderItemDTO implements DTOEntity{

	private int id;
	
	private int amount;
	
	private MealDTO meal;
	
	@JsonBackReference
	private OrderDTO order;

	public OrderItemDTO() {
		super();
	}

	public MealDTO getMeal() {
		return meal;
	}


	public void setMeal(MealDTO meal) {
		this.meal = meal;
	}


	public OrderDTO getOrder() {
		return order;
	}


	public void setOrder(OrderDTO order) {
		this.order = order;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}
}
