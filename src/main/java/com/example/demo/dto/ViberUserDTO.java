package com.example.demo.dto;

public class ViberUserDTO  implements DTOEntity{
	
	private String id;
	
	private String viberId;
	
	private String name;
	
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ViberUserDTO() {
		super();
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getViberId() {
		return viberId;
	}
	public void setViberId(String viberId) {
		this.viberId = viberId;
	}
	
}
