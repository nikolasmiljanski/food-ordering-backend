package com.example.demo.dto;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonBackReference;

public class MenuDTO implements DTOEntity{

		private int id;
		
		private List<MealDTO> meals;
		
		private byte[] picture;
		
		private LocalDate startDate;
		
		private LocalDate endDate;
		
		private MenuTypeDTO menuType;

		//private MenuDTO menu;
		
		//@JsonBackReference
	    private Set<MenuDTO> menus;
	    
		public MenuDTO() {
			super();
		}
		
		public byte[] getPicture() {
			return picture;
		}

		public void setPicture(byte[] picture) {
			this.picture = picture;
		}

		public LocalDate getStartDate() {
			return startDate;
		}

		public List<MealDTO> getMeals() {
			return meals;
		}

		public void setMeals(List<MealDTO> meals) {
			this.meals = meals;
		}

		public void setStartDate(LocalDate startDate) {
			this.startDate = startDate;
		}

		public LocalDate getEndDate() {
			return endDate;
		}

		public void setEndDate(LocalDate endDate) {
			this.endDate = endDate;
		}

//		public MenuDTO getMenu() {
//			return menu;
//		}
//
//		public void setMenu(MenuDTO menu) {
//			this.menu = menu;
//		}

		public Set<MenuDTO> getMenus() {
			return menus;
		}

		public void setMenus(Set<MenuDTO> menus) {
			this.menus = menus;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public MenuTypeDTO getMenuType() {
			return menuType;
		}

		public void setMenuType(MenuTypeDTO menuType) {
			this.menuType = menuType;
		}


}
