package com.example.demo.dto;

public class MealSubTypeDTO implements DTOEntity{

	private int id;

	private String name;

	public MealSubTypeDTO() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
