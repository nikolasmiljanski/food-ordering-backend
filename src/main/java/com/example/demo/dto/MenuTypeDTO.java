package com.example.demo.dto;

public class MenuTypeDTO implements DTOEntity{

	private int id;
	
	private String name;
	
	public MenuTypeDTO() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
