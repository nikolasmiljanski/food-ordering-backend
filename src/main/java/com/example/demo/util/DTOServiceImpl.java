
package com.example.demo.util;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.DTOEntity;


@Service
public class DTOServiceImpl implements DTOService {

	@Autowired
	private ModelMapper modelMapper;

	@Override
	public DTOEntity convertToDto(Object obj, DTOEntity mapper) {
		return modelMapper.map(obj,mapper.getClass());
	}

	@Override
	public Object convertToEntity(Object obj, DTOEntity mapper) {
		return modelMapper.map(mapper,obj.getClass());
	}

	@Override
	public List<DTOEntity> convertToDtoList(List<? extends Object> listObj, DTOEntity mapper) {
		List<DTOEntity> dtoEntityList = new ArrayList<DTOEntity>();
		for (Object obj : listObj) {
			DTOEntity dtoEntity = convertToDto(obj,mapper);
			dtoEntityList.add(dtoEntity);
		}
		return dtoEntityList;
	}

	@Override
	public List<? extends Object> convertToEntityList(Object object, List<DTOEntity> mapper) {
		List<Object> listObject = new ArrayList<Object>();
		for (DTOEntity dtoEntity : mapper) {
			Object tempObj = convertToEntity(object,dtoEntity);
			listObject.add(tempObj);
		}
		return listObject;
	}
}
