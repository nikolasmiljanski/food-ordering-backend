package com.example.demo.util;

import java.util.List;

import com.example.demo.dto.DTOEntity;


public interface DTOService {
	
	DTOEntity convertToDto(Object obj, DTOEntity mapper);
	Object convertToEntity(Object obj, DTOEntity mapper);
	
	List<DTOEntity> convertToDtoList(List<? extends Object> listObj, DTOEntity mapper);
	List<? extends Object> convertToEntityList(Object object,List<DTOEntity> mapper);
}
