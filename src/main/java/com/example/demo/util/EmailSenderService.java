package com.example.demo.util;

public interface EmailSenderService {
	public void sendEmail(String to, String subject, String text);
}
