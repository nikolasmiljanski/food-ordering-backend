package com.example.demo.util;

public enum MenuTypeEnum {
	DAILY("daily"),
	WEEKLY("weekly");
	
	private String label;
	
	MenuTypeEnum(String label){
		this.label=label;
	}

	public String getLabel() {
		return label;
	}
}
