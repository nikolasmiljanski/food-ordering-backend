package com.example.demo.viber;

import java.util.List;

public class BroadCastMessage {
	private List<String> broadcast_list;
	private String type;
	private int min_api_version;
	private Person sender;
	private String text;
	private String tracking_data;
	
	public BroadCastMessage() {
		super();
	}
	public BroadCastMessage(List<String> broadcast_list, String text) {
		super();
		this.min_api_version=1;
		this.tracking_data="tracking_data";
		this.broadcast_list = broadcast_list;
		this.type = "text";
		this.text = text;
		this.sender = new Person();
	}
	public List<String> getBroadcast_list() {
		return broadcast_list;
	}
	public void setBroadcast_list(List<String> broadcast_list) {
		this.broadcast_list = broadcast_list;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getMin_api_version() {
		return min_api_version;
	}
	public void setMin_api_version(int min_api_version) {
		this.min_api_version = min_api_version;
	}
	public Person getSender() {
		return sender;
	}
	public void setSender(Person sender) {
		this.sender = sender;
	}
	public String getTracking_data() {
		return tracking_data;
	}
	public void setTracking_data(String tracking_data) {
		this.tracking_data = tracking_data;
	}
	
}
