package com.example.demo.viber;

import java.util.List;

import com.example.demo.dto.ViberUserDTO;
import com.example.demo.model.ViberUser;

public interface ViberUserService {
	ViberUserDTO save(ViberUserDTO viberUserDto);
	
	ViberUserDTO save(String viberId);
	
	List<ViberUser> findAll();
	
	void sendDailyToAll();
	
	ViberUser findById(int id);

}
