package com.example.demo.viber;

public class MessageFromUser {

	private int id;
	private String idUser;
	private String	text;
	
	public MessageFromUser(String idUser, String text) {
		super();
		this.idUser = idUser;
		this.text = text;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	@Override
	public String toString() {
		return "Message [id=" + id + ", text=" + text + "]      " + idUser;
	}
	public String getIdUser() {
		return idUser;
	}
	public void setIdUser(String idUser) {
		this.idUser = idUser;
	}
	
}
