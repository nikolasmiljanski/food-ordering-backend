package com.example.demo.viber;

public class Person {
	private String name;
	
	private String avatar;

	public Person(String name, String avatar) {
		super();
		this.name = name;
		this.avatar = avatar;
	}

	public Person() {
		super();
		this.name="SimpleTaskBot";
		this.avatar=" ";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	
	
}
