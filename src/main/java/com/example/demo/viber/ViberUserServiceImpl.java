package com.example.demo.viber;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.demo.dto.ViberUserDTO;
import com.example.demo.model.Meal;
import com.example.demo.model.Menu;
import com.example.demo.model.ViberUser;
import com.example.demo.repository.ViberUserRepository;
import com.example.demo.service.DebterUserService;
import com.example.demo.service.MenuService;
import com.example.demo.util.DTOService;

@Service
public class ViberUserServiceImpl implements ViberUserService{

	@Autowired
	ViberUserRepository viberUserRepository;
	
	@Autowired
	MenuService menuService;
	
	@Autowired
	DebterUserService debterUserService;
	
	@Autowired
	DTOService dtoService;
	
	@Override
	public ViberUserDTO save(ViberUserDTO viberUserDto) {
		if(viberUserRepository.findByViberId(viberUserDto.getViberId())!=null) {
			return null;
		}
		ViberUser viberUser = (ViberUser) dtoService.convertToEntity(new ViberUser(), viberUserDto);
		viberUserRepository.save(viberUser);
		return (ViberUserDTO) dtoService.convertToDto(viberUser, new ViberUserDTO());
	}

	@Override
	public ViberUserDTO save(String viberId) {
		String viberIdReturn = viberId.substring(1,viberId.length()-1);
		if(viberUserRepository.findByViberId(viberIdReturn) != null) {
			return null;
		}
		ViberUser viberUser = new ViberUser();
		viberUser.setViberId(viberIdReturn);
		return (ViberUserDTO) dtoService.convertToDto(viberUserRepository.save(viberUser), new ViberUserDTO());
	}

	@Override
	public List<ViberUser> findAll() {
		return viberUserRepository.findAll();
	}
	
	public List<String> listIds(){
		List<ViberUser> listUser = findAll();
		List<String> listIds = new ArrayList<String>();
		for (ViberUser viberUser : listUser) {
			listIds.add(viberUser.getViberId());
		}
		return listIds;
	}

	@Override
	public void sendDailyToAll() {	
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("X-Viber-Auth-Token", "4b07cc81c4a7d1f4-13dba3a5c407c185-31b33823032b5143");
		RestTemplate restTemplate = new RestTemplate();
		BroadCastMessage sm = new BroadCastMessage(listIds(),toStringMeals());//listIds is a list of ViberId accounts
		HttpEntity<BroadCastMessage> request = new HttpEntity<>(sm,headers);
		restTemplate.postForObject("https://chatapi.viber.com/pa/broadcast_message", request, NormalMessage.class);
	}
	
	private String toStringMeals() {
		Menu menu = new Menu();
		try{
			menu = (Menu) dtoService.convertToEntity(new Menu(),menuService.findByDate(LocalDate.now()));
			List<Meal> meals = menu.getMeals();
			StringBuilder stringBuilder = new StringBuilder();
			for (Meal meal : meals) {
				System.out.println(meal);
				stringBuilder.append(meal.getName() + ", opis: " + meal.getDescription() + ", po ceni od: "+meal.getPrice() + " \n");
			}
			stringBuilder.append(" link ka stranici");
			return stringBuilder.toString();
		} catch(Exception e) {
			System.out.println("Nema jela");
		}
		return "Nema jela";
	}

	@Override
	public ViberUser findById(int id) {
		return viberUserRepository.findById(id).get();
	}
}