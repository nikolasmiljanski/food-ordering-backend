package com.example.demo.viber;

public class MessageResponse {
	String message_token;
	int status;
	String status_message;
	public MessageResponse(String message_token, int status, String status_message) {
		super();
		this.message_token = message_token;
		this.status = status;
		this.status_message = status_message;
	}
	
	
	@Override
	public String toString() {
		return "MessageResponse [message_token=" + message_token + ", status=" + status + ", status_message="
				+ status_message + "]";
	}


	public MessageResponse() {
		super();
	}


	public String getMessage_token() {
		return message_token;
	}
	public void setMessage_token(String message_token) {
		this.message_token = message_token;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getStatus_message() {
		return status_message;
	}
	public void setStatus_message(String status_message) {
		this.status_message = status_message;
	}
	   
}
