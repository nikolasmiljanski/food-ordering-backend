package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.OrderItemDTO;
import com.example.demo.model.OrderItem;

public interface OrderItemService {
	
	OrderItem save(OrderItem orderItem);
	
	List<OrderItem> save(List<OrderItem> orderItems);
	
	void delete(OrderItemDTO orderItemDto);
}
