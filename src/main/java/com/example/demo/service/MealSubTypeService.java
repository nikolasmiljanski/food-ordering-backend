package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.DTOEntity;
import com.example.demo.dto.MealSubTypeDTO;

public interface MealSubTypeService {
	
	public MealSubTypeDTO save(MealSubTypeDTO mealSubType);
	
	public void delete(int id);
	
	List<DTOEntity> findAll();
}
