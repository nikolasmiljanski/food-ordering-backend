package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.DTOEntity;
import com.example.demo.dto.DebterUserDTO;
import com.example.demo.model.DebterUser;
import com.example.demo.model.User;
import com.example.demo.repository.DebterUserRepository;
import com.example.demo.util.DTOService;

@Service
public class DebterUserServiceImpl implements DebterUserService{

	@Autowired
	DebterUserRepository debterUserRepository;
	
	@Autowired
	DTOService dtoService;

	@Override
	public DebterUser findByUser(User u) {
		return debterUserRepository.findByUser(u);
	}

	@Override
	public DebterUser save(DebterUser debterUser) {
		return debterUserRepository.save(debterUser);
	}
	
	private DebterUserDTO save(DebterUserDTO debterUserDto) {
		if(debterUserDto == null) return null;
		DebterUser debterUser = (DebterUser) dtoService.convertToEntity(new DebterUser(), debterUserDto);
		return (DebterUserDTO) dtoService.convertToDto(save(debterUser), new DebterUserDTO());
	}

	@Override
	public List<DTOEntity> findAll() {
		List<DebterUser> debterUsers = debterUserRepository.findAll();
		if(debterUsers == null) {
			return null;
		}
		return dtoService.convertToDtoList(debterUsers, new DebterUserDTO());
	}
	
	@Override
	public void delete(DebterUserDTO debterUserDto) {
		if(debterUserDto==null)
			return;
		DebterUser debterUser = (DebterUser) dtoService.convertToEntity(new DebterUser(), debterUserDto);
		debterUserRepository.delete(debterUser);
	}

	@Override
	public DebterUserDTO paid(int id, boolean paid) {
		DebterUser debterUser = debterUserRepository.findById(id).get();
		if(debterUser==null) return null;
		debterUser.setIsPaid(paid);
		return (DebterUserDTO) dtoService.convertToDto(save(debterUser), new DebterUserDTO());
	}
	
}
