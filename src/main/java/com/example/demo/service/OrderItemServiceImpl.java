package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.OrderDTO;
import com.example.demo.dto.OrderItemDTO;
import com.example.demo.model.Order;
import com.example.demo.model.OrderItem;
import com.example.demo.repository.OrderItemRepository;
import com.example.demo.util.DTOService;

@Service
public class OrderItemServiceImpl implements OrderItemService {

	@Autowired
	DTOService dtoService;
	
	@Autowired
	OrderItemRepository orderItemRepository;
	
	@Autowired
	OrderService orderService;
	
	@Override
	public OrderItem save(OrderItem orderItem) {
		return orderItemRepository.save(orderItem);
	}

	@Override
	public List<OrderItem> save(List<OrderItem> orderItems) {
		List<OrderItem> orderItemReturn = new ArrayList<>();
		for (OrderItem orderItem : orderItems) {
			orderItemReturn.add(orderItemRepository.save(orderItem));
		}
		return orderItemReturn;
	}

	@Override
	public void delete(OrderItemDTO orderItemDto) {
		OrderItem orderItem = (OrderItem) dtoService.convertToEntity(new OrderItem(), orderItemDto);
		Order order = orderItem.getOrder();
		order.getOrderItems().remove(orderItem);
		orderService.saveOrder((OrderDTO) dtoService.convertToDto(order, new OrderDTO()));
		orderItemRepository.delete(orderItem);
	}

}
