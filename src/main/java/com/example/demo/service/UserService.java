package com.example.demo.service;
import java.util.List;

import com.example.demo.dto.DTOEntity;
import com.example.demo.dto.UserDTO;
import com.example.demo.model.User;

public interface UserService {

	void ignoreOrder(User u);
	
	public void ignoreMenuList(List<User> listUser);
	
	User findById(int id);
	
	List<DTOEntity> findAll();

	User findOne(int id);

	UserDTO save(UserDTO userDto);

    User findByEmail(String email);
    
    UserDTO getChosenOne();
    
    UserDTO register(UserDTO userDto);
    
    UserDTO login(UserDTO userDto);
    
    UserDTO activateUser(int id);
    
    UserDTO deactivateUser(int id);
   
    boolean viberUser(int idUser, int idViber);

}