package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.DTOEntity;
import com.example.demo.dto.MealDTO;
import com.example.demo.model.Meal;
import com.example.demo.repository.MealRepository;
import com.example.demo.util.DTOService;


@Service
public class MealServiceImpl implements MealService{

	@Autowired
	DTOService dtoService;
	
	@Autowired
	MealRepository mealRepository;
	
	@Override
	public List<DTOEntity> findAll() {
		return dtoService.convertToDtoList(mealRepository.findAll(), new MealDTO());
	}

	@Override
	public void delete(int id) {
		 mealRepository.deleteById(id);
	}

	@Override
	public MealDTO save(MealDTO mealDto) {
		Meal m = mealRepository.save((Meal) dtoService.convertToEntity(new Meal(), mealDto));
		return (MealDTO) dtoService.convertToDto(m ,new MealDTO());
	}

	@Override
	public Meal findById(int id) {
		
		return mealRepository.findById(id).get();
	}

}