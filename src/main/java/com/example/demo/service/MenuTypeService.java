package com.example.demo.service;

import com.example.demo.model.MenuType;

public interface MenuTypeService {
	
	MenuType save(MenuType menuType);
	
	MenuType findByName(String menuType);
}
