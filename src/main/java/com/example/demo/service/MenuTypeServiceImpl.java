package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.MenuType;
import com.example.demo.repository.MenuTypeRepository;
import com.example.demo.util.DTOService;

@Service
public class MenuTypeServiceImpl implements MenuTypeService {

	@Autowired
	MenuTypeRepository menuTypeRepository;
	
	@Autowired
	DTOService dtoService;
	
	@Override
	public MenuType save(MenuType menuType) {
		return menuTypeRepository.save(menuType);
	}

	@Override
	public MenuType findByName(String menuType) {
		return menuTypeRepository.findByName(menuType);
	}


}
