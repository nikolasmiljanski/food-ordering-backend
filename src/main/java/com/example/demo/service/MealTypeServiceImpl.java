package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.DTOEntity;
import com.example.demo.dto.MealTypeDTO;
import com.example.demo.model.MealType;
import com.example.demo.repository.MealTypeRepository;
import com.example.demo.util.DTOService;

@Service
public class MealTypeServiceImpl implements MealTypeService{

	@Autowired
	MealTypeRepository mealTypeRepository;
	
	@Autowired
	DTOService dtoService;
	
	@Override
	public List<DTOEntity> findAll() {
		return dtoService.convertToDtoList(mealTypeRepository.findAll(), new MealTypeDTO());
	}
	
	@Override
	public MealTypeDTO save(MealTypeDTO mealTypeDto) {
		MealType m = mealTypeRepository.save((MealType) dtoService.convertToEntity(new MealType(), mealTypeDto));
		return (MealTypeDTO) dtoService.convertToDto(m ,new MealTypeDTO());
	}

	@Override
	public void delete(int id) {
		mealTypeRepository.deleteById(id);
	}

	@Override
	public MealType findByName(String mealType) {
		return mealTypeRepository.findByName(mealType);
	}
}

