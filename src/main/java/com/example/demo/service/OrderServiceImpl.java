package com.example.demo.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.DTOEntity;
import com.example.demo.dto.OrderDTO;
import com.example.demo.dto.UserDTO;
import com.example.demo.model.DebterUser;
import com.example.demo.model.Order;
import com.example.demo.model.OrderItem;
import com.example.demo.model.User;
import com.example.demo.repository.OrderRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.util.DTOService;

@Service
public class OrderServiceImpl implements OrderService {

	@Autowired
	OrderRepository orderRepository;
	
	@Autowired
	DebterUserService debterUserService;
	
	@Autowired
	OrderItemService orderItemService;
	
	@Autowired
	MealService mealService;
	
	@Autowired
	DTOService dtoService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	UserRepository userRepository;
	
	
	@Override
	public OrderDTO saveOrder(OrderDTO orderDto) {
		Order order = (Order) dtoService.convertToEntity(new Order(),orderDto);
		List<OrderItem> listOrderItems = orderItemService.save(order.getOrderItems());
		Order orderReturn = orderRepository.save(order);
		for (OrderItem orderItem : listOrderItems) {
			orderItem.setOrder(order);
			orderItem.setMeal(mealService.findById(orderItem.getMeal().getId()));
		}
		orderItemService.save(listOrderItems);
		User user = userService.findById(order.getUser().getId());
		user.getOrders().add(order);
		userService.save((UserDTO) dtoService.convertToDto(user, new UserDTO()));
		assignToDebter(user,getSum(order.getOrderItems()));
		userService.ignoreOrder(orderReturn.getUser());
		return (OrderDTO) dtoService.convertToDto(orderReturn, new OrderDTO());
	}
	
	private void assignToDebter(User u,int sum) {
		DebterUser debterUser = debterUserService.findByUser(u);
		if(debterUser !=null) {
			debterUser.setPrice(debterUser.getPrice()+sum);
			debterUserService.save(debterUser);
			return;
		} else {
			debterUser = new DebterUser();
			debterUser.setUser(u);
			debterUser.setPrice(sum);
			debterUserService.save(debterUser);
		}
	}

	private int getSum(List<OrderItem> orderItems) {
		int rez = 0;
		for (OrderItem orderItem : orderItems) {
			rez += orderItem.getMeal().getPrice()*orderItem.getAmount();
		}
		return rez;
	}
	
	@Override
	public List<DTOEntity> findAll() {
		return dtoService.convertToDtoList(orderRepository.findAll(), new OrderDTO());
	}

	@Override
	public void delete(int id) {
		orderRepository.delete(orderRepository.findById(id).get());	
	}

	@Override
	public List<DTOEntity> findToday() {
		List<Order> orders = orderRepository.findByDate(LocalDate.now());
		for (Order eachOrder : orders) {
			if(eachOrder.getUser() != null)
				userService.ignoreOrder(eachOrder.getUser());			
		}
		return dtoService.convertToDtoList(orderRepository.findByDate(LocalDate.now()), new OrderDTO());
	}
	
	@Override
	public List<DTOEntity> findByUser(int idUser) {
		User user = userService.findById(idUser);
		List<Order> orders = orderRepository.findByUser(user);
		if (orders == null)
			return null;
		return dtoService.convertToDtoList(orders, new OrderDTO());
	}
}
