package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.DTOEntity;
import com.example.demo.dto.MealDTO;
import com.example.demo.model.Meal;

public interface MealService {

	Meal findById(int id);
	
	List<DTOEntity> findAll();

	void delete(int id);

	MealDTO save(MealDTO mealDto);

}
