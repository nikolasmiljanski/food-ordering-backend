package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.DTOEntity;
import com.example.demo.dto.MealTypeDTO;
import com.example.demo.model.MealType;

public interface MealTypeService {
	
	MealTypeDTO save(MealTypeDTO mealTypeDto);
	
	MealType findByName(String mealType);
	
	List<DTOEntity> findAll();
	
	void delete(int id);
}

