package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.DTOEntity;
import com.example.demo.dto.OrderDTO;


public interface OrderService {
	
	OrderDTO saveOrder(OrderDTO o);
	
	List<DTOEntity> findAll();
	
	void delete(int id);
	
	List<DTOEntity> findToday();
	
	List<DTOEntity> findByUser(int id);
}

