package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.DTOEntity;
import com.example.demo.dto.MealSubTypeDTO;
import com.example.demo.model.MealSubType;
import com.example.demo.repository.MealSubTypeRepository;
import com.example.demo.util.DTOService;

@Service
public class MealSubTypeServiceImpl implements MealSubTypeService {

	@Autowired
	DTOService dtoService;
	
	@Autowired
	MealSubTypeRepository mealSubTypeRepository;
	
	@Override
	public List<DTOEntity> findAll() {
		return dtoService.convertToDtoList(mealSubTypeRepository.findAll(), new MealSubTypeDTO());
	}

	@Override
	public void delete(int id) {
		 mealSubTypeRepository.delete(mealSubTypeRepository.findById(id).get());
	}

	@Override
	public MealSubTypeDTO save(MealSubTypeDTO mealSubTypeDto) {
		MealSubType m = mealSubTypeRepository.save((MealSubType) dtoService.convertToEntity(new MealSubType(), mealSubTypeDto));
		return (MealSubTypeDTO) dtoService.convertToDto(m ,new MealSubTypeDTO());
	}

}
