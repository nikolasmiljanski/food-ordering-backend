package com.example.demo.service;

import java.time.LocalDate;
import java.util.List;

import com.example.demo.dto.DTOEntity;
import com.example.demo.dto.MenuDTO;
import com.example.demo.model.Menu;

public interface MenuService {
	
	//List<DTOEntity> findByMenu(Menu m);
	
	Menu findById(int id);
	
	MenuDTO saveDaily(MenuDTO menuDto);
	
	MenuDTO saveWeekly(MenuDTO menuDto);
	
	MenuDTO findThisWeek();
	
	MenuDTO findByDate(LocalDate date);
	
	MenuDTO findToday();
	
	List<DTOEntity> findAll();
	
	List<DTOEntity> findAllDailyMenus();
	
	List<DTOEntity> listWeekly();
	
	//void ignoreMenuList(List<Menu> listMenu);

	//void ignoreMenu(Menu menu);

//	public List<DTOEntity> findByMenu(Menu m);
}
