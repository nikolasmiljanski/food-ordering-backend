package com.example.demo.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.demo.dto.DTOEntity;
import com.example.demo.dto.UserDTO;
import com.example.demo.model.Order;
import com.example.demo.model.Role;
import com.example.demo.model.User;
import com.example.demo.model.ViberUser;
import com.example.demo.repository.UserRepository;
import com.example.demo.util.DTOService;
import com.example.demo.util.RoleEnum;
import com.example.demo.viber.ViberUserService;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	ViberUserService viberUserService;
	
	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleService roleService;

	@Autowired
	DTOService dtoService;

	@Autowired
	OrderService orderService;

	@Autowired
	UserService userService;

	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;

	@Override
	public User findOne(int id) {
		return userRepository.getOne(id);
	}

	@Override
	public User findByEmail(String email) {
		return userRepository.findByEmail(email);

	}

	@Override
	public List<DTOEntity> findAll() {
		return dtoService.convertToDtoList(userRepository.findAll(), new UserDTO());
	}
	
	@Override
	public void ignoreMenuList(List<User> listUser) {
		for (int i = 0; i < listUser.size(); i++) {
			listUser.get(i).setOrders(null);
		}
	}

	private String extractNameFromEmail(String email) {
		String[] emailSplited = email.split("@");
		String name = emailSplited[0];
		return name;
	}
	
	@Override
	public User findById(int id) {
		return userRepository.findById(id).get();
	}

	public void ignoreOrder(User u) {
		u.setOrders(null);
	}

	@Override
	public UserDTO getChosenOne() {
		User user = userRepository.findByRole(roleService.findByName(RoleEnum.CHOSEN.name()));
		if(user != null) {
			return (UserDTO) dtoService.convertToDto(user, new UserDTO());
		}
		
		List<DTOEntity> ordersDto = orderService.findToday();
		List<Order> orders = (List<Order>) dtoService.convertToEntityList(new Order(), ordersDto);
		Set<User> users = new HashSet<>();
		for (Order order : orders) {

			users.add(order.getUser());

		}
		List<User> listUser = new ArrayList<User>(users);
		if (listUser.size() == 0) {
			return null;
		}
		User returnUser = listUser.get(0);
		Role role = roleService.findByName(RoleEnum.CHOSEN.name());
		role.getUsers().add(returnUser);
		roleService.save(role);
		returnUser.getRoles().add(role);
		return (UserDTO) dtoService.convertToDto(userRepository.save(returnUser), new UserDTO());		
	}

	@Override
	public UserDTO register(UserDTO userDto) {
		User user =(User) dtoService.convertToEntity(new User(),userDto);
		if(userRepository.findByEmail(user.getEmail())!= null) {
 			return null;
 		}
		Role role = roleService.findByName(RoleEnum.USER.name());
 		user.setRoles(new HashSet<Role>());
 		user.getRoles().add(role);
 		user.setCreationDate(LocalDate.now());
 		user.setActive(true);
 		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
 		return (UserDTO) dtoService.convertToDto(userRepository.save(user),new UserDTO());
	}

	@Override
	public UserDTO login(UserDTO userDto) {
		
		User user = userRepository.findByEmail(userDto.getEmail());
		if(!bCryptPasswordEncoder.matches(userDto.getPassword(), user.getPassword())) {
				return null;
		}
		if(user == null || !user.isActive()) {
			return null;
		}
		return (UserDTO) dtoService.convertToDto(user, new UserDTO());
	}

	@Override
	public UserDTO save(UserDTO userDto) {
		User user = userRepository.save((User) dtoService.convertToEntity(new User(), userDto));
		return (UserDTO) dtoService.convertToDto(user, new UserDTO());
	}

	@Override
	public boolean viberUser(int idUser, int idViber) {
		User user = findById(idUser);
		ViberUser viberUser = viberUserService.findById(idViber);
		user.setViberUser(viberUser);
		UserDTO userDto = save((UserDTO) dtoService.convertToDto(user, new UserDTO()));
		if(viberUser != null && userDto != null) {
			return true;
		}
		return false;
	}

	@Override
	public UserDTO activateUser(int id) {
		User user = findById(id) ;
		user.setActive(true);
		
		UserDTO userDto = save((UserDTO) dtoService.convertToDto(user, new UserDTO()));
		if (userDto == null)
			return null;
		else
			return userDto;
	}

	@Override
	public UserDTO deactivateUser(int id) {
		User user = findById(id) ;
		user.setActive(false);
		
		UserDTO userDto = save((UserDTO) dtoService.convertToDto(user, new UserDTO()));
		if (userDto == null)
			return null;
		else
			return userDto;
	}
	
	
}