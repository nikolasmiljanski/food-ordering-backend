package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.DTOEntity;
import com.example.demo.dto.DebterUserDTO;
import com.example.demo.model.DebterUser;
import com.example.demo.model.User;

public interface DebterUserService {
	
	DebterUser findByUser(User u);
	
	DebterUser save(DebterUser debterUser);
	
	List<DTOEntity> findAll();
	
	void delete(DebterUserDTO debterUserDto);
	
	DebterUserDTO paid(int id, boolean paid);

}
