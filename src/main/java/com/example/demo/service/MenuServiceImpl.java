package com.example.demo.service;

import java.time.LocalDate;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.dto.DTOEntity;
import com.example.demo.dto.MenuDTO;

import com.example.demo.util.MenuTypeEnum;
import com.example.demo.model.Menu;
import com.example.demo.repository.MenuRepository;
import com.example.demo.util.DTOService;

@Service
public class MenuServiceImpl implements MenuService {

	@Autowired
	MenuRepository menuRepository;

	@Autowired
	DTOService dtoService;

	@Autowired
	MenuTypeService menuTypeService;

	@Override
	public Menu findById(int id) {
		return menuRepository.findById(id).get();
	}

	@Override
	public MenuDTO saveDaily(MenuDTO menuDto) {
		Menu m =(Menu) dtoService.convertToEntity(new Menu(), menuDto);
		if(findByDate(m.getStartDate())!= null) {
			Menu tmp = (Menu) dtoService.convertToEntity(new Menu(), findByDate(m.getStartDate()));
			tmp.setMeals(m.getMeals());
			m = tmp;
		}
		m.setEndDate(m.getStartDate());
		return (MenuDTO) dtoService.convertToDto(menuRepository.save(m) ,new MenuDTO());
	}

	@Override
	public MenuDTO saveWeekly(MenuDTO menuDto) {
		Menu menu = menuRepository.save((Menu) dtoService.convertToEntity(new Menu(), menuDto));
		return (MenuDTO) dtoService.convertToDto(menu, new MenuDTO());
	}

	@Override
	public List<DTOEntity> listWeekly() {
		List<DTOEntity> list = dtoService.convertToDtoList(menuRepository.findByMenuType(menuTypeService.findByName(MenuTypeEnum.WEEKLY.getLabel())),new MenuDTO());
		return list;
	}

	@Override
	public MenuDTO findThisWeek() {
		List<Menu> menus = menuRepository.findByDateAround(LocalDate.now());
		Menu returnMenu = null;
		for (Menu menu : menus) {
			if (menu.getMenuType().getName().equals(MenuTypeEnum.WEEKLY.getLabel())) {
				returnMenu = menu;
			}
		}

		if(returnMenu == null)
			return null;
		return (MenuDTO) dtoService.convertToDto(returnMenu, new MenuDTO());
	}

	@Override
	public MenuDTO findToday() {
		return findByDate(LocalDate.now());
	}

	@Override
	public List<DTOEntity> findAll() {
		return dtoService.convertToDtoList(menuRepository.findAll(), new MenuDTO());
	}

	@Override
	public List<DTOEntity> findAllDailyMenus() {
		return dtoService.convertToDtoList(menuRepository.findByMenuType(menuTypeService.findByName(MenuTypeEnum.DAILY.getLabel())),new MenuDTO());
	}

	@Override
	public MenuDTO findByDate(LocalDate date) {
		List<Menu> menus = menuRepository.findByDate(date);
		for (Menu menu : menus) {
			if(menu.getMenuType().getName().equals(MenuTypeEnum.DAILY.getLabel())) {
				return (MenuDTO) dtoService.convertToDto(menu, new MenuDTO());
			}
		}
		return null;
	}

//	@Override
//	public void ignoreMenuList(List<Menu> listMenu) {
//		for (int i = 0; i < listMenu.size(); i++) {
//			listMenu.get(i).setMenu(null);
//		}
//	}
//
//	@Override
//	public void ignoreMenu(Menu menu) {
//		menu.setMenu(null);
//	}
//
//	@Override
//	public List<DTOEntity> findByMenu(Menu m) {
//
//		return dtoService.convertToDtoList(menuRepository.findByMenu(m), new MenuDTO());
//
//	}
}
