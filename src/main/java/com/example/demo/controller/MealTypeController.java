package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.DTOEntity;
import com.example.demo.dto.MealTypeDTO;
import com.example.demo.service.MealTypeService;

@RestController
public class MealTypeController {

	@Autowired
	MealTypeService mealTypeService;

	@PostMapping(value = "/admin/mealtype")
	public ResponseEntity<DTOEntity> saveMealType(@RequestBody MealTypeDTO mealTypeDto) {
		MealTypeDTO returnMealType = mealTypeService.save(mealTypeDto);
		if(returnMealType==null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(returnMealType, HttpStatus.CREATED);
		}
	}
	
	@GetMapping("/admin/mealtype")
	ResponseEntity<List<DTOEntity>> getMealTypes(){
		return new ResponseEntity<>(mealTypeService.findAll(),HttpStatus.ACCEPTED);
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@GetMapping("/mealtype")
	ResponseEntity<List<DTOEntity>> mealType() {
		List<DTOEntity> mealType = mealTypeService.findAll();
		return new ResponseEntity<>(mealType, HttpStatus.ACCEPTED);
	}

	@DeleteMapping("/admin/mealtype/{id}")
	ResponseEntity<Integer> deleteType(@PathVariable(name = "id") int idMealType) {
		mealTypeService.delete(idMealType);
		return new ResponseEntity<>(HttpStatus.ACCEPTED);
	}
}
