package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.DTOEntity;
import com.example.demo.dto.UserDTO;
import com.example.demo.service.UserService;
import com.example.demo.util.DTOService;

@RestController
public class UserController {
	@Autowired
	UserService userService;

	@Autowired
	DTOService dtoService;

	@CrossOrigin(origins = "http://localhost:3000")
	@PostMapping(path = "/register")
	public ResponseEntity<DTOEntity> register(@RequestBody UserDTO userDto) {
		UserDTO returnUser = userService.register(userDto);
		if (returnUser == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(returnUser, HttpStatus.CREATED);

	}
	
	@CrossOrigin(origins = "http://localhost:3000")
	@PostMapping(path = "/login")
	public ResponseEntity<DTOEntity> login(@RequestBody UserDTO userDto) {
		UserDTO returnUser = userService.login(userDto);
		if (returnUser == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(returnUser, HttpStatus.OK);
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@GetMapping("/chosenone")
	public ResponseEntity<DTOEntity> getChosen() {
		UserDTO userDto = userService.getChosenOne();
		if (userDto == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<>(userDto, HttpStatus.ACCEPTED);
	}
	
	@CrossOrigin(origins = "http://localhost:3000")
	@GetMapping("/getallusers")
	public ResponseEntity<List<DTOEntity>> getAllUsers() {
		List<DTOEntity> userDTOs = userService.findAll();
		if (userDTOs == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<>(userDTOs, HttpStatus.ACCEPTED);
	}
	
	@CrossOrigin(origins = "http://localhost:3000")
	@GetMapping("/activate/{id}")
	public ResponseEntity<List<DTOEntity>> activateUser(@PathVariable(value="id") int id) {
		UserDTO user = userService.activateUser(id);
		if (user != null) {	
			return new ResponseEntity<>(HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}
	
	@CrossOrigin(origins = "http://localhost:3000")
	@GetMapping("/deactivate/{id}")
	public ResponseEntity<List<DTOEntity>> deactivateUser(@PathVariable(value="id") int id) {
		UserDTO user = userService.deactivateUser(id);
		if (user != null) {	
			return new ResponseEntity<>(HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}
	
//	@PostMapping(value="/viberuser")
//	public ResponseEntity<User> getMessageText(@RequestParam int idUser,@RequestParam int idViber) {
//		userService.viberUser(idUser,idViber);
//		return new ResponseEntity<T>(HttpStatus.CREATED);
//	}

}
