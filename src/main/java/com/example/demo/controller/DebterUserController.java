package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.DTOEntity;
import com.example.demo.dto.DebterUserDTO;
import com.example.demo.service.DebterUserService;

@RestController
public class DebterUserController {
	
	@Autowired
	DebterUserService debterUserService;
	@CrossOrigin(origins = "http://localhost:3000")
	@GetMapping("/getdebters")
	public ResponseEntity<List<DTOEntity>> getDebters(){
		List<DTOEntity> debters = debterUserService.findAll();
		if(debters == null)
			return new ResponseEntity<List<DTOEntity>>(HttpStatus.BAD_REQUEST);
		return new ResponseEntity<List<DTOEntity>>(debters,HttpStatus.OK);
	}
	@CrossOrigin(origins = "http://localhost:3000")
	@DeleteMapping("/deletedebter")
	public void deleteOrderItem(@RequestBody DebterUserDTO debterUserDto) {
		debterUserService.delete(debterUserDto);
	}
	
	@CrossOrigin(origins = "http://localhost:3000")
	@PostMapping("/toggle/{id}")
	public ResponseEntity<DebterUserDTO> paid(@PathVariable final int id, @RequestBody boolean isPaid){
		DebterUserDTO debterUserDto = debterUserService.paid(id, isPaid);
		if(debterUserDto == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(debterUserDto,HttpStatus.OK);
	}
	
}
