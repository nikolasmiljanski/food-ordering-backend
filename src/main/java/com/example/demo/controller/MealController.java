package com.example.demo.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.DTOEntity;
import com.example.demo.dto.MealDTO;
import com.example.demo.service.MealService;

@RestController
public class MealController {

	@Autowired
	MealService mealService;

	@CrossOrigin(origins = "http://localhost:3000")
	@GetMapping("/meal")
	List<DTOEntity> allMeals() {
		return mealService.findAll();
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@PostMapping("/meal")
	ResponseEntity<DTOEntity> newMeal(@RequestBody MealDTO mealDto) {
		MealDTO returnMeal = mealService.save(mealDto);
		if (returnMeal == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(returnMeal, HttpStatus.OK);
	}
	 
	@DeleteMapping("/meal/{id}")
	void deleteMeals(@PathVariable int id) {
		mealService.delete(id);
	}
}
