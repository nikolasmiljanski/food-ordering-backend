package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.DTOEntity;
import com.example.demo.dto.ViberUserDTO;
import com.example.demo.viber.MessageFromUser;
import com.example.demo.viber.ViberUserService;

@RestController
public class ViberController {
	
	@Autowired
	ViberUserService viberUserService;
	
	@PostMapping(value="/subscribed")
	public ResponseEntity<DTOEntity> getId(@RequestBody ViberUserDTO viberUserDto) {
		return new ResponseEntity<>(viberUserService.save(viberUserDto), HttpStatus.CREATED);
	}
	@PostMapping(value="/receivemessage")
	public ResponseEntity<MessageFromUser> getMessageText(@RequestBody MessageFromUser text) {
		return new ResponseEntity<>(text, HttpStatus.CREATED);
	}
	
	
	
	@Scheduled(cron = "*/20 * * * * ?")
	public void metod() {
		viberUserService.sendDailyToAll();;
	}
	
}
