package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.DTOEntity;
import com.example.demo.dto.OrderDTO;
import com.example.demo.service.OrderService;

@RestController
public class OrderController {

	@Autowired
	OrderService orderService;
	
	@CrossOrigin(origins = "http://localhost:3000")
	@PostMapping("/saveorder")
	public ResponseEntity<DTOEntity> saveOrder(@RequestBody OrderDTO orderDto){
		OrderDTO returnOrder = orderService.saveOrder(orderDto);
		if(returnOrder == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(returnOrder,HttpStatus.CREATED);
	}
	
	@DeleteMapping("/orders/{id}")
	public ResponseEntity<OrderDTO> deleteOrder(@PathVariable(name="id")int idOrder){
		orderService.delete(idOrder);
		return new ResponseEntity<>(HttpStatus.ACCEPTED);
	}
	
	@GetMapping("/orders")
	public ResponseEntity<List<DTOEntity>> getOrders(){
		return new ResponseEntity<>(orderService.findAll(),HttpStatus.ACCEPTED);
	}
	
	@GetMapping("/todayorders")
	public ResponseEntity<List<DTOEntity>> getTodayOrders(){
		return new ResponseEntity<>(orderService.findToday(),HttpStatus.ACCEPTED);
	}
	
	@CrossOrigin(origins = "http://localhost:3000")
	@GetMapping("/orders/{id}")
	public ResponseEntity<List<DTOEntity>> getUsersOrder(@PathVariable(name="id")int idUser){
		List<DTOEntity> ordersDto = orderService.findByUser(idUser);
		if(ordersDto==null)
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		
		return new ResponseEntity<>(ordersDto,HttpStatus.ACCEPTED);
	}
}
