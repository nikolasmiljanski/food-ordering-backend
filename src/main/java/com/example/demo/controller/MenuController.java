package com.example.demo.controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.DTOEntity;
import com.example.demo.dto.MenuDTO;
import com.example.demo.service.MenuService;

@RestController
public class MenuController {

	@Autowired
	MenuService menuService;
	
	@GetMapping("/finddays")
	ResponseEntity<DTOEntity> daysOfDate(@RequestBody LocalDate localDate){
		DTOEntity menuDto = menuService.findByDate(localDate);
		if (menuDto==null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(menuDto, HttpStatus.CREATED);
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@GetMapping("/admin/menu/weekly")
	ResponseEntity<List<DTOEntity>> weeklyMenu() {
		return new ResponseEntity<>(menuService.listWeekly(), HttpStatus.ACCEPTED);
	}
	
	@CrossOrigin(origins = "http://localhost:3000")
	@PostMapping("/admin/menu/daily/add")
	ResponseEntity<DTOEntity> addMenu(@RequestBody MenuDTO menuDto) {
		MenuDTO returnMenu = menuService.saveDaily(menuDto);
		if (returnMenu == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(returnMenu,HttpStatus.OK);
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@GetMapping("/admin/menu/thisweek")
	ResponseEntity<DTOEntity> thisWeek(){
		MenuDTO week = menuService.findThisWeek();
		if(week==null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(week,HttpStatus.ACCEPTED);
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@GetMapping("/admin/menu/today")
	ResponseEntity<MenuDTO> today(){
		MenuDTO daily = menuService.findToday();
		if(daily==null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(daily,HttpStatus.ACCEPTED);
	}
	
	@CrossOrigin(origins = "http://localhost:3000")
	@GetMapping("/admin/dailymenus")
	ResponseEntity<List<DTOEntity>> getAllDailyMenus() {
		List<DTOEntity> daily = menuService.findAllDailyMenus();

		if (daily == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(daily, HttpStatus.ACCEPTED);
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@PostMapping("/menu")
	ResponseEntity<DTOEntity> addWeekly(@RequestBody MenuDTO menuDto) {
		menuService.saveWeekly(menuDto);
		if (menuDto == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(menuDto, HttpStatus.CREATED);
	}
}
