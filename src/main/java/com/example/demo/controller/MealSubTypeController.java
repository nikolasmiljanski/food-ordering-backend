package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.DTOEntity;
import com.example.demo.dto.MealSubTypeDTO;
import com.example.demo.service.MealSubTypeService;

@RestController
public class MealSubTypeController {

	
	@Autowired
	MealSubTypeService mealSubTypeService;
	
	@CrossOrigin(origins = "http://localhost:3000")
	@GetMapping("/mealsubtype")
	List<DTOEntity> allMealSubTypes() {
		return mealSubTypeService.findAll();
	}
	
	@PostMapping("/mealsubtype")
	ResponseEntity<DTOEntity> newMealSubType(@RequestBody MealSubTypeDTO mealSubTypeDto) {
		MealSubTypeDTO returnMealSubType = mealSubTypeService.save(mealSubTypeDto);
		if (returnMealSubType == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(returnMealSubType, HttpStatus.CREATED);
	}
	 
	@DeleteMapping("/mealsubtype/{id}")
	void deleteMealSubType(@PathVariable int id) {
		mealSubTypeService.delete(id);
	}
}
