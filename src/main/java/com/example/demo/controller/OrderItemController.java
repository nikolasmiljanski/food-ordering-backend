package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.OrderItemDTO;
import com.example.demo.service.OrderItemService;

@RestController
public class OrderItemController {
	
	@Autowired
	OrderItemService orderItemService;
	
	@DeleteMapping("/orderItem")
	public void deleteOrderItem(@RequestBody OrderItemDTO orderItemDto) {
		orderItemService.delete(orderItemDto);
	}
}
