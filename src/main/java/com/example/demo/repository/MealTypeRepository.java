package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.MealType;

public interface MealTypeRepository extends JpaRepository<MealType, Integer>{

	MealType findByName(String name);
	


}


