package com.example.demo.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.model.Menu;
import com.example.demo.model.MenuType;

@Repository
public interface MenuRepository extends JpaRepository<Menu, Integer> {
	
	//List<Menu> findByMenu(Menu m);
	
	List<Menu> findByMenuType(MenuType menuType);
	
	@Query("SELECT m FROM Menu m WHERE m.startDate <= ?1 AND m.endDate > ?1")
	List<Menu> findByDateAround(LocalDate d);
	
	@Query("SELECT m FROM Menu m WHERE m.startDate = ?1")
	List<Menu> findByDate(LocalDate d);

}
