package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.DebterUser;
import com.example.demo.model.User;

public interface DebterUserRepository extends JpaRepository<DebterUser, Integer> {
	DebterUser findByUser(User u);
}
