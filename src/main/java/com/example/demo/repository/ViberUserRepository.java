package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.ViberUser;

public interface ViberUserRepository extends JpaRepository<ViberUser, Integer> {
	ViberUser findByViberId(String id);
}
