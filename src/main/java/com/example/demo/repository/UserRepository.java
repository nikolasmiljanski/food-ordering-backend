package com.example.demo.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.model.Role;
import com.example.demo.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
	User findByEmail(String Email);
	
	User findByEmailAndPassword(String email, String password);
	
	@Query("SELECT u FROM User u WHERE EXISTS(SELECT r FROM u.roles r WHERE r = ?1 )")
	User findByRole(Role r);
}
