package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.MealSubType;

public interface MealSubTypeRepository extends JpaRepository<MealSubType, Integer>{

}
