package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.model.MenuType;

@Repository
public interface MenuTypeRepository extends JpaRepository<MenuType, Integer>{
	
	MenuType findByName(String name);
}
