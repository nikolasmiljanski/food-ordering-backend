--
insert into food_ordering.user(active,creation_date,email,password,name) values(true,CURDATE(),"a.a@gmail.com","$2y$10$isZPDpk1d3G97lt10ZN.4uZ7RfN0o/LY0k1YW7AmvHTv7N5zuFlJG","a");
insert into food_ordering.user(active,creation_date,email,password,name) VALUES (true,curDate(),"test.test@gmail.com", "$2y$10$isZPDpk1d3G97lt10ZN.4uZ7RfN0o/LY0k1YW7AmvHTv7N5zuFlJG","test.test");
insert into food_ordering.user(active,creation_date,email,password,name) VALUES (true,curDate(),"test.test2@test.com", "$2y$10$isZPDpk1d3G97lt10ZN.4uZ7RfN0o/LY0k1YW7AmvHTv7N5zuFlJG","test.test");

insert into food_ordering.roles(name) values("ADMIN");
insert into food_ordering.roles(name) values("USER");
insert into food_ordering.roles(name) values("CHOSEN");

insert into food_ordering.mealsubtype(name) values("BIG");
insert into food_ordering.mealsubtype(name) values("SMALL");

insert into food_ordering.users_roles values(1,1);
insert into food_ordering.users_roles values(2,2);
insert into food_ordering.users_roles values(3,2);

insert into food_ordering.mealtype(name) values ("regular");
insert into food_ordering.mealtype(name) values ("fit");
insert into food_ordering.menutype(name) values ("weekly");
insert into food_ordering.menutype(name) values ("daily");


ALTER TABLE `food_ordering`.`menu_menus`
DROP INDEX `UK_fwwb9t19tk0va1yi4ooqso4tu` ;
